# Skewed Aspect Game Engine

This is a highly-opinionated game engine wrapping several technologies:

* [BabylonJS][babylon]
* [Electron][electron]
* [Typescript][typescript]

[babylon]: https://www.babylonjs.com/
[electron]: https://electronjs.org/
[typescript]: https://www.typescriptlang.org/

## Installation

Simply install sage into a blank node project:

`npm`
```bash
$ npm install -D @skewedaspect/sage
```

`yarn`
```bash
$ yarn add -D @skewedaspect/sage
```

Next, add a `app.ts` or `app.js` file:

```typescript
import path from 'path';
import { App } from '@skewedaspect/sage';

const app = new App({
    name: 'Example Game',
    fullscreen: true,
    devTools: true,
    vsync: false,
    gameScript: path.resolve(__dirname, 'game')
});

app.start();
```
