//----------------------------------------------------------------------------------------------------------------------
// SageApplication
//----------------------------------------------------------------------------------------------------------------------

import { app, BrowserWindow } from 'electron';

// Windows
import { mainWindow } from './windows/main';

// Interfaces
import { SageApplicationConfig, CustomFont } from './interfaces/app';

//----------------------------------------------------------------------------------------------------------------------

/**
 * The main application class.
 *
 * This class is the main interface with the underlying electron application. It's not intended to be extended (though
 * nothing would directly prevent it). It's main purpose is to pass in initial configuration as well as to specify what
 * file to load for the game class.
 */
export class SageApplication
{
    constructor(config : SageApplicationConfig)
    {
        this.name = config.name;
        this.gameScript = config.gameScript;
        this.fullscreen = config.fullscreen ?? true;
        this.width = config.width ?? 800;
        this.height = config.height ?? 600;
        this.frame = config.frame ?? true;
        this.vsync = config.vsync ?? true;
        this.devTools = config.devTools ?? false;
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------

    private readonly gameScript : string;
    private readonly name : string;
    private readonly fullscreen : boolean = true;
    private readonly width : number = 800;
    private readonly height : number = 600;
    private readonly frame : boolean = true;
    private readonly vsync : boolean = true;
    private readonly devTools : boolean = false;
    private mainWindow ?: BrowserWindow;

    /**
     * A list of all the currently added custom fonts.
     */
    public fonts : CustomFont[] = [];

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Creates the electron main window.
     */
    private createWindow() : void
    {
        // Create the main window
        this.mainWindow = new BrowserWindow({
            width: this.width,
            height: this.height,
            frame: this.frame,
            fullscreen: this.fullscreen,
            webPreferences: {
                nodeIntegration: true
            }
        });

        // Load our main window html
        this.mainWindow
            .loadURL(`data:text/html;base64,${ mainWindow(this.generateFontCSS()).toString('base64') }`)
            .then(() =>
            {
                this.mainWindow?.webContents.send('load-game', this.gameScript);
            });

        // Open the DevTools, if requested.
        if(this.devTools)
        {
            this.mainWindow.webContents.openDevTools();
        } // end if

        // Emitted when the window is closed.
        this.mainWindow.on('closed', () =>
        {
            // Dereference the window object.
            this.mainWindow = undefined;
        });
    } // end createWindow

    /**
     * Generates a `style` tag with custom css to load any registered custom fonts.
     *
     * @returns Returns a string html blob.
     */
    private generateFontCSS() : string
    {
        let styleTag = '<style id="custom-fonts">';

        styleTag += this.fonts.reduce((results, font) =>
        {
            results += `
                @font-face {
                    font-family: '${ font.family }';
                    src: url('${ font.path }') format('${ font.format }');
                    font-weight: ${ font.weight };
                    font-style: ${ font.style };
                }
            `;
            return results;
        }, '');

        return `${ styleTag }</style>`;
    } // end generateFontCSS

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Register a custom font to be loaded in the electron main window at start up.
     *
     * @param family - The name of the font family, i.e. 'Arial', 'Helvetica New', etc.
     *
     * @param path - The path on the file system to the file. _(Note: Currently not relative to `assetsRootDir`.)_
     *
     * @param format - The format of the fonts, i.e. 'truetype', 'woff2', etc. Any valid format string for `@font-face`.
     *
     * @param weight - The weight, as per `font-weight` in `@font-face`.
     *
     * @param style - The style, as per `font-style` in `@font-face`.
     */
    public addFont(family, path, format = 'truetype', weight = 'normal', style = 'normal') : void
    {
        this.fonts.push({ family, path, format, weight, style });
    } // end addFont

    /**
     * Starts the electron application.
     */
    public async start() : Promise<void>
    {
        // Disable electron security warnings; we're only loading local content, and not ever displaying remote content.
        // **DO NOT FUCK THIS UP.**
        process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = 'true';

        // Setup the app
        app.name = this.name;
        app.applicationMenu = null;

        // Disable the forced vsync in most webgl.
        if(!this.vsync)
        {
            app.commandLine.appendSwitch('disable-frame-rate-limit');
        } // end if

        // Create the window once electron's ready.
        app.on('ready', this.createWindow.bind(this));

        // Quit when all windows are closed.
        app.on('window-all-closed', () => app.quit());
    } // end start
} // end SageApplication

//----------------------------------------------------------------------------------------------------------------------
