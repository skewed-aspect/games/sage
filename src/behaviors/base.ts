//----------------------------------------------------------------------------------------------------------------------
// SageBehaviorBase
//----------------------------------------------------------------------------------------------------------------------

// Managers
import behaviorMan from '../managers/behavior';
import engineMan from '../managers/engine';

// Models
import { SageEntity } from '../entity';

//----------------------------------------------------------------------------------------------------------------------

export class SageBehaviorBase
{
    constructor(entity : SageEntity)
    {
        this.entity = entity;

        // Bind Lifecycle hooks
        engineMan.on('before render', this.beforeRendered.bind(this));
        engineMan.on('render', this.rendered.bind(this));
        engineMan.on('after render', this.afterRendered.bind(this));
        engineMan.on('fixed update', this.fixedUpdate.bind(this));
        this.entity.on('initialized', this.initialized.bind(this));
        this.entity.on('updated', this.updated.bind(this));
        this.entity.on('before destroy', this.beforeDestroyed.bind(this));
        this.entity.on('message', this.onMessage.bind(this));
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Protected Properties
    //------------------------------------------------------------------------------------------------------------------

    protected readonly entity : SageEntity;

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    public readonly behaviorName : string = 'base';

    //------------------------------------------------------------------------------------------------------------------
    // Lifecycle Hooks
    //------------------------------------------------------------------------------------------------------------------

    public async initialized() : Promise<void>
    {
        return undefined;
    } // end initialized

    public async beforeRendered() : Promise<void>
    {
        return undefined;
    } // end beforeRendered

    public async rendered() : Promise<void>
    {
        return undefined;
    } // end rendered

    public async afterRendered() : Promise<void>
    {
        return undefined;
    } // end afterRendered

    public async fixedUpdate(_timeDelta : number) : Promise<void>
    {
        return undefined;
    } // end fixedUpdate

    public async updated() : Promise<void>
    {
        return undefined;
    } // end updated

    public async beforeDestroyed() : Promise<void>
    {
        return undefined;
    } // end beforeDestroyed

    //------------------------------------------------------------------------------------------------------------------
    // Message handler
    //------------------------------------------------------------------------------------------------------------------

    public async onMessage(_data : { [ key : string ] : any }) : Promise<void>
    {
        return undefined;
    } // end onMessage
} // end SageBehaviorBase

//----------------------------------------------------------------------------------------------------------------------

behaviorMan.register('base', SageBehaviorBase);

//----------------------------------------------------------------------------------------------------------------------
