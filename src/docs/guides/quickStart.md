# Quick Start

This is how you get started with a SAGE project. First, create a new node project from scratch:

`npm`
```bash
$ mkdir my-project
$ cd my-project
$ npm init
```

`yarn`
```bash
$ mkdir my-project
$ cd my-project
$ yarn init
```


## Installation

Next, install SAGE:

`npm`
```bash
$ npm install -D @skewedaspect/sage
```

`yarn`
```bash
$ yarn add -D @skewedaspect/sage
```

## Main Application file (`app.ts`)

Now, add a `app.ts` or `app.js` file. (Typescript is encouraged, but not required.) This is the main entrypoint for the
electron application. Any code put here will run in electron's `main` process, _not the `render` process_, so please be
aware of that.

_**This file is not where your game logic belongs; it's primarily book keeping for electron. Most projects will not have
much more in this than the example.**_

```typescript
import path from 'path';
import { App } from '@skewedaspect/sage';

const app = new App({
    name: 'Example Game',
    fullscreen: true,
    devTools: true,
    vsync: false,
    gameScript: path.resolve(__dirname, 'game')
});

app.start();
```

## Main Game file (`game.ts`)

Next, you will add a `game.ts` or `game.js` file. This file is the entry point for the actual game logic. It runs in
electron's `render` process (or in the Babylon context, "in the browser"). 

```typescript
import path from 'path';

// Scenes
import { TestScene } from './scenes/test';

// Base Models
import { GameBase, sceneMan } from '../sage';

class ExampleGame extends GameBase
{
    async start(canvas : HTMLCanvasElement) : Promise<void>
    {
        await super.start(canvas, { assetRootDir: path.resolve(__dirname, 'assets') });

        // Register scenes
        sceneMan.register('test', TestScene);

        // Load the initial scene
        sceneMan.load('test');
    } // end start
} // end ExampleGame

export default new ExampleGame();
```

This file is the entry point for all game logic, and may end up looking drastically different depending on your project
layout, or preferred code structure. The only important pieces are:

1. The module creates a class that extends `GameBase`.
2. If you have initialization, you override `start` can call `super.start` immediately, or you wait on the `'engine
   started'` event from the engine manager.
3. The module exports a new instance of your game class as the default export. (Or, in the case of a CommonJS module, `module.exports`.)

That's it!

## Where to go from here

We recommend learning more about the different managers available, or learning about the entity system next.
