## v1.0.0:

* [X] Electron app management
    * [X] Handle electron app creation/window management
    * [X] Custom Font Support
* [X] EngineManager
    * [X] Ability to create engine
    * [X] Render hooks (`before render`, `render`, `after render`)
    * [X] Start/Stop render loop
* [ ] SceneManager
    * [X] Ability to load scenes from files.
    * [X] Registration of scenes, or some other way to load scenes by 'name'.
    * [X] Mesh instancing.
    * [ ] Progress reporting.
* [ ] EntityManager
    * [ ] Ability to load entity from definition (JSON?)
    * [ ] Defined behaviors
    * [ ] Ability to serialize entity (JSON?)
    * [ ] Ability to load entity meshes into the loaded scene.
* [ ] InputManager
    * [ ] Device support (mouse, keyboard, gamepad)
    * [ ] Inputs:
        * [ ] Analog
        * [ ] Digital
    * [ ] Outputs:
        * [ ] Momentary (Discrete)
        * [ ] Switch (Discrete)
        * [ ] Axis (Continuous)
    * [ ] Actions
    * [ ] Ability to load input mappings
* [ ] UIManager
    * [ ] Decide: Vue or BabylonGUI??
    * [ ] Ability to define a theme
    * [ ] Custom controls / widgets
    * [ ] XML Loader extension (if using BabylonGUI)
