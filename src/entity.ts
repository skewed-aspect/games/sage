//----------------------------------------------------------------------------------------------------------------------
// SageEntity
//----------------------------------------------------------------------------------------------------------------------

import { v4 } from 'uuid';
import Emittery = require('emittery');

// Managers
import behaviorMan from './managers/behavior';

// Base Classes
import { SageBehaviorBase } from './behaviors';

//----------------------------------------------------------------------------------------------------------------------

/**
 * Entity Manager events that return data, with their associated data types.
 */
type TypedBaseEntityEvents = {
    message : { [ key : string ] : any };
};

/**
 * Entity Manager events that do not return data.
 */
type VoidBaseEntityEvents = 'initialized' | 'updated' | 'before destroy';

//----------------------------------------------------------------------------------------------------------------------

/**
 * Represents a game 'entity'.
 *
 * In SAGE, an 'entity' is anything that can have behaviors attached to it. Entities contain game logic, and (generally)
 * have models associated with them. They are the building blocks of game logic in SAGE.
 */
export class SageEntity extends Emittery.Typed<TypedBaseEntityEvents, VoidBaseEntityEvents>
{
    constructor(type : string, state : { [ key : string ] : any })
    {
        super();

        this.id = state.id ?? v4();
        this.type = type ?? 'base';
        this.behaviors = new Set();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    /**
     * The unique identifier for the entity. Must be globally unique. By default, will be a uuid v4.
     */
    public readonly id : string;

    /**
     * The type of entity this is. By default, will be 'base'.
     */
    public readonly type : string;

    /**
     * Any attached behaviors.
     */
    public readonly behaviors : Set<SageBehaviorBase>;

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Attach a new behavior to the entity. This can be either a string or an array of strings, where the name
     * corresponds to a registered behavior name. A new instance is created every time. You cannot add more than one
     * instance of a particular type of behavior.
     *
     * @param behaviorName - The behavior name or a list of behavior names to add.
     */
    public attachBehavior(behaviorName : string | string[]) : void
    {
        if(Array.isArray(behaviorName))
        {
            behaviorName.map((item) => this.attachBehavior(item));
            return;
        } // end if

        // Pull this out and check for undefined first
        const maybeBehavior = behaviorMan.get(behaviorName);
        if(!maybeBehavior)
        {
            throw new Error(`Unknown behavior name '${ behaviorName }'.`);
        } // end if

        // Change the variable so we can reuse it
        const BehaviorConstructor = maybeBehavior;

        // Check to see if we already have a behavior of this type attached to the entity.
        const alreadyExists = Array.from(this.behaviors).some((behavior) =>
        {
            return behavior.behaviorName === behaviorName;
        });

        if(alreadyExists)
        {
            throw new Error(`Behavior '${ behaviorName }' already attached to entity.`);
        } // end if

        const behaviorInstance = new BehaviorConstructor(this);
        this.behaviors.add(behaviorInstance);
    } // end attachBehavior

    /**
     * Called when the entity is initialized by the manager. Primarily for any sub-classes that need async
     * initialization.
     *
     * @returns Returns a promise that resolves once all initialization (and event handlers)
     */
    public async init() : Promise<void>
    {
        return this.emit('initialized');
    } // end init

    /**
     * Updates the state of the entity.
     *
     * @param _state - The state to apply over top of the entity's state.
     *
     * @returns Returns a promise that resolves once the state has been applied, and all event listeners have resolved.
     */
    public async update(_state : { [ key : string ] : any }) : Promise<void>
    {
        return this.emit('updated');
    } // end update

    /**
     * Destroys the entity, and cleans up any resources associated with it.
     *
     * @returns Returns a promise that resolves once the entity has been removed, and all event listeners have resolved.
     */
    public async destroy() : Promise<void>
    {
        await this.emit('before destroy');
    } // end destroy
} // end SageEntity

//----------------------------------------------------------------------------------------------------------------------
