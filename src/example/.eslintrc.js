module.exports = {
    env: {
        browser: true
    },
    rules: {
        // Bad Practices
        'no-console': [
            "error",
            { allow: [ "debug", "info", "warn", "error" ] }
        ],
    }
};
