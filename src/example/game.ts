// ---------------------------------------------------------------------------------------------------------------------
// Example Game
// ---------------------------------------------------------------------------------------------------------------------

import path from 'path';

// Scenes
import { TestScene } from './scenes/test';

// Base Models
import { GameBase, sceneMan } from '../sage';

// ---------------------------------------------------------------------------------------------------------------------

class ExampleGame extends GameBase
{
    async start(canvas : HTMLCanvasElement) : Promise<void>
    {
        await super.start(canvas, { assetRootDir: path.resolve(__dirname, 'assets') });

        // Register scenes
        sceneMan.register('test', TestScene);

        // Load the initial scene
        sceneMan.load('test');
    } // end start
} // end ExampleGame

// ---------------------------------------------------------------------------------------------------------------------

export default new ExampleGame();

// ---------------------------------------------------------------------------------------------------------------------
