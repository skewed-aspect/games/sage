//----------------------------------------------------------------------------------------------------------------------
// TestScene
//----------------------------------------------------------------------------------------------------------------------

import { engineMan, SceneBase } from '../../sage';
import { Color3, FreeCamera, HemisphericLight, MeshBuilder, Vector3 } from 'babylonjs';
import * as GUI from 'babylonjs-gui';

//----------------------------------------------------------------------------------------------------------------------

export class TestScene extends SceneBase
{
    public async load() : Promise<void>
    {
        // Create a FreeCamera, and set its position to (x:0, y:5, z:-10).
        const camera = new FreeCamera('camera1', new Vector3(0, 5, -10), this.scene);

        // Target the camera to scene origin.
        camera.setTarget(Vector3.Zero());

        // Attach the camera to the canvas.
        camera.attachControl(engineMan.canvas, false);

        // Create a basic light, aiming 0,1,0 - meaning, to the sky.
        const light = new HemisphericLight('light1', new Vector3(0, 1, 0), this.scene);
        light.intensity = 0.25;
        light.groundColor = Color3.Purple();

        // Our built-in 'sphere' shape.
        const sphere = MeshBuilder.CreateSphere('sphere', { diameter: 2, segments: 32 }, this.scene);

        // Move the sphere upward 1/2 its height
        sphere.position.y = 1;

        // Our built-in 'ground' shape.
        const ground = MeshBuilder.CreateGround('ground', { width: 6, height: 6 }, this.scene);
        ground.position.z = 0;

        const gui = GUI.AdvancedDynamicTexture.CreateFullscreenUI('myUI');
        const dpr = window.devicePixelRatio;
        const rect = engineMan.canvas.getBoundingClientRect();
        gui.idealWidth = rect.width;
        gui.renderScale = dpr;

        const button1 = GUI.Button.CreateSimpleButton('button', 'Start Game');
        button1.top = '-60px';
        button1.left = '0px';
        button1.width = '150px';
        button1.height = '50px';
        button1.cornerRadius = 12 * dpr * dpr;
        button1.thickness = 4 * dpr * dpr;
        button1.children[0].color = '#FFFFFF';
        button1.children[0].fontSize = 24;
        button1.children[0].fontFamily = 'Titillium Web';
        button1.color = '#0060a0';
        button1.background = '#0099FF';

        gui.addControl(button1);

        const button = GUI.Button.CreateSimpleButton('button', 'Exit');
        button.top = '0px';
        button.left = '0px';
        button.width = '150px';
        button.height = '50px';
        button.cornerRadius = 12 * dpr * dpr;
        button.thickness = 4 * dpr * dpr;
        button.children[0].color = '#FFFFFF';
        button.children[0].fontSize = 24;
        button.children[0].fontFamily = 'Titillium Web';
        button.color = '#0060a0';
        button.background = '#0099FF';

        button.onPointerClickObservable.add(() =>
        {
            require('electron').remote.app.quit();
        });

        gui.addControl(button);
    } // end load
} // end TestScene

//----------------------------------------------------------------------------------------------------------------------
