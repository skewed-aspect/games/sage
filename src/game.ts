// ---------------------------------------------------------------------------------------------------------------------
// Base Game class. All games should inherit from this class!
// ---------------------------------------------------------------------------------------------------------------------

// Managers
import engineMan from './managers/engine';
import stateMan from './managers/state';

// Interfaces
import { SageGameOptions } from './interfaces/game';

// Behaviors
import './behaviors';

// ---------------------------------------------------------------------------------------------------------------------

/**
 * The base game class.
 *
 * In Sage, everything starts with the game class. Users inherit from this class and add their own code into an
 * override of the `start` function. Sage will call this function when it starts, and execute whatever code you've
 * written.
 *
 * The game class really is nothing more then a useful convention for starting the game; you don't need to try to fit
 * all your logic in it, or even use it beyond the `start` function. But it's there to have a consistent, common
 * entrypoint for all Sage games.
 */
export class SageGameBase
{
    /**
     * The game's start function is the function that Sage calls once the main window has finished initializing.
     *
     * The user is expected to override this to add their own code, however, _calls to Sage managers cannot be safely
     * made until after the `engineMan` is initialized_, which the base implementation of start does. Therefore, it's
     * _highly_ recommended to include `super.start(canvas, options)` as the first line of your override.
     *
     * **Only put code before calling the base implementation's `start` if you know what you're doing!**
     *
     * @param canvas - The html canvas entity for babylon to draw to.
     *
     * @param options - Options that control things like the rendering, or the setup of the game.
     *
     * @returns Returns a promise that returns once the game has been fully started.
     */
    public async start(canvas : HTMLCanvasElement, options : SageGameOptions) : Promise<void>
    {
        stateMan.set('assetRootDir', options.assetRootDir);

        // Initialize Managers
        engineMan.init(canvas, options.highDPI);

        // Start the engine
        engineMan.start();
    } // end start
} // end SageGameBase

// ---------------------------------------------------------------------------------------------------------------------
