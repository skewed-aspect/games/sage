// ---------------------------------------------------------------------------------------------------------------------
// Application interfaces
// ---------------------------------------------------------------------------------------------------------------------

/**
 * The main application configuration.
 */
export interface SageApplicationConfig {
    name : string;
    gameScript : string;
    fullscreen ?: boolean;
    width ?: number;
    height ?: number;
    frame ?: boolean;
    vsync ?: boolean;
    devTools ?: boolean;
}

/**
 * Represents a custom font.
 */
export interface CustomFont {
    family : string;
    path : string;
    format : string;
    weight : string;
    style : string;
}

// ---------------------------------------------------------------------------------------------------------------------
