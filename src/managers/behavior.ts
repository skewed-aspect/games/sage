//----------------------------------------------------------------------------------------------------------------------
// BehaviorManager
//----------------------------------------------------------------------------------------------------------------------

import { SageBehaviorConstructor } from '../interfaces/behavior';

//----------------------------------------------------------------------------------------------------------------------

/**
 * The Behavior Manager
 *
 * This manager is for accessing (and registering) various behaviors to be applied to entities.
 */
class BehaviorManager
{
    constructor()
    {
        this.behaviors = new Map();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    public readonly behaviors : Map<string, SageBehaviorConstructor>;

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Gets a behavior constructor by name.
     *
     * @param behaviorName - The name of the behavior to get.
     *
     * @returns Returns the desired behavior constructor, or undefined if a behavior by that name wasn't found.
     */
    public get(behaviorName : string) : SageBehaviorConstructor | undefined
    {
        return this.behaviors.get(behaviorName);
    } // end get

    /**
     * Register a behavior constructor by name.
     *
     * @param behaviorName - The name to register the behavior constructor under.
     * @param BehaviorConstructor - The behavior constructor.
     */
    public register(behaviorName : string, BehaviorConstructor : SageBehaviorConstructor) : void
    {
        this.behaviors.set(behaviorName, BehaviorConstructor);
    } // end register
} // end BehaviorManager

//----------------------------------------------------------------------------------------------------------------------

export default new BehaviorManager();

//----------------------------------------------------------------------------------------------------------------------
