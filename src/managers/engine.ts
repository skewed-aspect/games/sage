//----------------------------------------------------------------------------------------------------------------------
// EngineManager
//----------------------------------------------------------------------------------------------------------------------

import { Engine } from 'babylonjs';
import Emittery = require('emittery');

//----------------------------------------------------------------------------------------------------------------------

const FIXED_INTERVAL = 1000 / 30; // (30 fps)

/**
 * Engine Manager events that return data, with their associated data types.
 */
type TypedEngineEvents = { 'fixed update' : number };

/**
 * Engine Manager events that do not return data.
 */
type VoidEngineEvents = 'before render' | 'render' | 'after render' | 'engine started' | 'engine stopped';

//----------------------------------------------------------------------------------------------------------------------

/**
 * The BabylonJS Engine manager.
 *
 * This handles setting up the core 3D engine, as well as providing event hooks for
 */
class EngineManager extends Emittery.Typed<TypedEngineEvents, VoidEngineEvents>
{
    constructor()
    {
        super();

        window.requestAnimationFrame(this._onAnimationFrame.bind(this));
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Private Properties
    //------------------------------------------------------------------------------------------------------------------

    private _engine ?: Engine;
    private _canvas ?: HTMLCanvasElement;
    private _lastTick : number = Date.now();

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    /**
     * If the engine is currently started, or not.
     */
    public started = false;

    /**
     * A read-only reference to the game's html canvas element. Throws an error if accessed before the manager is
     * initialized.
     *
     * @returns Returns the html canvas element the game was started with.
     */
    public get canvas() : HTMLCanvasElement
    {
        if(!this._canvas)
        {
            throw new Error('Attempted to retrieve canvas before manager initialized.');
        } // end if

        return this._canvas;
    } // end canvas

    /**
     * A read-only reference to the [[Engine]] instance. Throws an error if accessed before the manager is initialized.
     *
     * @returns Returns the game's engine instance.
     */
    public get engine() : Engine
    {
        if(!this._engine)
        {
            throw new Error('Attempted to retrieve engine before manager initialized.');
        } // end if

        return this._engine;
    } // end engine

    //------------------------------------------------------------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------------------------------------------------------------

    private async _renderLoop() : Promise<void>
    {
        // Emitted before the current scene is rendered
        await this.emit('before render');

        // Emitted to trigger a scene render
        await this.emit('render');

        // Emitted after the scene has rendered.
        await this.emit('after render');
    } // end _renderLoop

    private _onAnimationFrame(domTimestamp : DOMHighResTimeStamp) : void
    {
        // Schedule the next frame
        window.requestAnimationFrame(this._onAnimationFrame.bind(this));

        const timeDelta = this._lastTick - domTimestamp;
        if(timeDelta >= FIXED_INTERVAL)
        {
            this._lastTick = domTimestamp;
            this.emit('fixed update', timeDelta);
        } // end if
    } // end _onAnimationFrame

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Initializes the manager. This is **required** to be called before it is safe to access any other Sage manager. (
     * This is because most depend on the engine, and access the engine before we get passed the canvas will cause
     * managers to have the wrong engine instance. So, we throw an exception if that's attempted.)
     *
     * @param canvas - The html canvas to perform drawing operations on.
     *
     * @param highDPI - Should we support device pixel rations greater than 1? (Defaults to true.) Turn this off if
     * performance is an issue on highDPI devices.
     */
    public init(canvas : HTMLCanvasElement, highDPI = true) : void
    {
        if(this._engine)
        {
            throw new Error('Attempted to initialize manager twice.');
        } // end if

        this._canvas = canvas;
        this._engine = new Engine(canvas, true, {}, highDPI);

        // The canvas/window resize event handler.
        window.addEventListener('resize', () => this._engine?.resize());
    } // end init

    /**
     * Starts the engine's render loop. This must be called in order for scenes to be rendered!
     */
    public start() : void
    {
        if(!this._engine)
        {
            throw new Error('Attempted to start engine before manager initialized.');
        } // end if

        if(this.started)
        {
            throw new Error('Attempted to start an already started engine.');
        } // end if

        this.started = true;
        this._engine.runRenderLoop(this._renderLoop.bind(this));
        this.emit('engine started');
    } // end start

    /**
     * Stops the engine's render loop.
     */
    public stop() : void
    {
        if(!this._engine)
        {
            throw new Error('Attempted to stop engine before manager initialized.');
        } // end if

        if(!this.started)
        {
            throw new Error('Attempted to stop an already stopped engine.');
        } // end if

        this.started = false;
        this._engine.stopRenderLoop();
        this.emit('engine stopped');
    } // end stop
} // end EngineManager

//----------------------------------------------------------------------------------------------------------------------

export default new EngineManager();

//----------------------------------------------------------------------------------------------------------------------
