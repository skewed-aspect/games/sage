//----------------------------------------------------------------------------------------------------------------------
// EntityManager
//----------------------------------------------------------------------------------------------------------------------

import Emittery = require('emittery');

// Interfaces
import { EntityConstructor } from '../interfaces/entity';

// Base Classes
import { SageEntity } from '../entity';

//----------------------------------------------------------------------------------------------------------------------

/**
 * Entity Manager events that return data, with their associated data types.
 */
type TypedEntityEvents = {
    'created' : string;
    'destroyed' : string;
};

/**
 * Entity Manager events that do not return data.
 */
type VoidEntityEvents = '';

//----------------------------------------------------------------------------------------------------------------------

/**
 * The Entity Manager
 *
 * This manager is for creating and managing entities. It allows for creation, removal, querying, and the ability to
 * broadcast messages to entities.
 */
class EntityManager extends Emittery.Typed<TypedEntityEvents, VoidEntityEvents>
{
    constructor()
    {
        super();

        this.entities = new Map();
        this.constructors = new Map();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    /**
     * The currently loaded entities.
     */
    public entities : Map<string, SageEntity>;

    /**
     * The registered entities that we can construct.
     */
    public constructors : Map<string, EntityConstructor>;

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Register an entity constructor as a particular string. (This allows for that string to be referenced as the
     * entity 'type' when creating an entity.
     *
     * @param entityType - The string to register as the entity type.
     * @param constructor - The entity constructor to register.
     */
    public register(entityType : string, constructor : EntityConstructor) : void
    {
        this.constructors.set(entityType, constructor);
    } // end register

    /**
     * Creates an entity of the given type with the provided state.
     *
     * @param entityType - The name of the entity type.
     * @param state - The initial state to create the entity with.
     *
     * @returns Returns the newly created entity.
     */
    public async create(entityType : string, state : { [ key : string ] : any }) : Promise<SageEntity>
    {
        const Entity = this.constructors.get(entityType);
        if(!Entity)
        {
            console.warn(`Unknown entity type '${ entityType }'. Ignoring.`);
            throw new Error(`Unknown entity type '${ entityType }'. Ignoring.`);
        }
        else
        {
            // Construct and initialize new entity.
            const entityInstance = new Entity(entityType, state);
            await entityInstance.init();

            // Store this for future lookup.
            this.entities.set(entityInstance.id, entityInstance);

            // Fire event
            this.emit('created', entityInstance.id);

            // Return this, just to be nice.
            return entityInstance;
        } // end if
    } // end create

    /**
     * Retrieve an entity by id.
     *
     * @param entityID - The id of the entity.
     *
     * @returns Returns the entity, if found, otherwise returns `undefined`.
     */
    public get(entityID : string) : SageEntity | undefined
    {
        return this.entities.get(entityID);
    } // end get

    /**
     * Gets a list of entities by entity type.
     *
     * @param entityType - The entity type to retrieve.
     *
     * @returns Returns a list of entities, if any match the type, otherwise returns an empty list.
     */
    public getByType(entityType : string) : SageEntity[]
    {
        const matched : SageEntity[] = [];
        if(!this.constructors.has(entityType))
        {
            console.warn(`Unknown entity type '${ entityType }'. No matches.`);
        }
        else
        {
            this.entities.forEach((value) =>
            {
                if(value.type === entityType)
                {
                    matched.push(value);
                } // end if
            });
        } // end if

        return matched;
    } // end getByType

    /**
     * Destroys (deletes) a specific entity.
     *
     * @param entityID - The ID of the entity to delete.
     *
     * @returns Returns a promise that resolves once the entity has successfully been destroyed. (Meaning all
     * callbacks have returned.)
     */
    public async destroy(entityID : string) : Promise<void>
    {
        const entity = this.get(entityID);
        if(!entity)
        {
            console.warn(`Unknown entity with id'${ entityID }'.`);
            throw new Error(`Unknown entity with id'${ entityID }'.`);
        }
        else
        {
            await entity.destroy();
            this.entities.delete(entityID);

            // Fire event
            this.emit('destroyed', entityID);
        } // end if
    } // end destroy

    /**
     * Sends an event to either a single entity, or a group of entities. (This is useful for passing game logic events
     * to entities, or for networked entities.)
     *
     * @param name - The name of the event.
     * @param data - Any data associated with the event (passed in as a single parameter).
     * @param entityID - The entity id, or string of ids to send the message to.
     */
    public fireEvent(name : string, data : { [ key : string ] : any }, entityID : string | string[]) : void
    {
        if(Array.isArray(entityID))
        {
            // Fire to a list of entities
            entityID.map((ent) => this.fireEvent(name, data, ent));
        }
        else if(!entityID)
        {
            // Broadcast to all entities
            this.entities.forEach((ent) => this.fireEvent(name, data, ent.id));
        }
        else
        {
            const ent = this.entities.get(entityID);
            if(ent)
            {
                ent.emit('message', data);
            } // end if
        } // end if
    } // end fireEvent
} // end EntityManager

//----------------------------------------------------------------------------------------------------------------------

export default new EntityManager();

//----------------------------------------------------------------------------------------------------------------------
