//----------------------------------------------------------------------------------------------------------------------
// SceneManager
//----------------------------------------------------------------------------------------------------------------------

import 'babylonjs-loaders';
import Emittery = require('emittery');

// Managers
import engineMan from './engine';

// Base class for scenes
import { SageSceneBase } from '../scene';
import { SceneConstructor } from '../interfaces/scene';

//----------------------------------------------------------------------------------------------------------------------

/**
 * Scene Manager events that return data, with their associated data types.
 */
type TypedSceneEvents = { 'scene changed' : SageSceneBase | undefined };

/**
 * Scene Manager events that do not return data.
 */
type VoidSceneEvents = '';

//----------------------------------------------------------------------------------------------------------------------

/**
 * The BabylonJS Scene manager.
 *
 * This manager is specifically for registering and loading scenes. It handles all the details of creating scenes, and
 * switching out the currently rendering scene.
 */
class SceneManager extends Emittery.Typed<TypedSceneEvents, VoidSceneEvents>
{
    constructor()
    {
        super();

        // Hold a list of registered scenes
        this.scenes = new Map();

        // Bind events
        engineMan.on('render', this._render.bind(this));
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Private Properties
    //------------------------------------------------------------------------------------------------------------------

    private scenes : Map<string, SageSceneBase>;
    private _currentScene ?: SageSceneBase;

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    /**
     * The currently loaded scene, if any.
     *
     * @returns Returns the current scene.
     */
    public get currentScene() : SageSceneBase | undefined
    {
        return this._currentScene;
    }

    /**
     * Sets the current scene, and emits an event if the scene changes.
     *
     * @param val - The new scene to set.
     */
    public set currentScene(val : SageSceneBase | undefined)
    {
        if(val !== this._currentScene)
        {
            this._currentScene = val;
            this.emit('scene changed', val);
        } // end if
    }

    //------------------------------------------------------------------------------------------------------------------
    // Event Handlers
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Render hook for rendering the current scene, if one is loaded.
     *
     * @private
     */
    private _render() : void
    {
        if(this.currentScene)
        {
            this.currentScene.render();
        } // end if
    } // end _render

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Registers a scene with the scene manager. (You can register the same scene under multiple names, but they will be
     * multiple instances of the same scene.)
     *
     * @param sceneName - The name to register this scene under. Calls to `load` will use this name.
     * @param Scene - The class to instantiate for the scene. _(Note: this is not an instance; it must be the class!)_
     */
    public register(sceneName : string, Scene : SceneConstructor) : void
    {
        this.scenes.set(sceneName, new Scene());
    } // end register

    /**
     * Loads a scene. This involves checking to see if we have a scene registered under that name, loading the scene if
     * it hasn't been loaded before, and then setting the current scene to the new scene.
     *
     * Because we lazily load scenes (we don't call `load()` until the first time the scene is loaded) changing to a
     * new scene will take longer the first time it's loaded. From then on, however, it's cached.
     *
     * @param sceneName - The name of the new scene to load.
     *
     * @returns Returns the newly loaded scene.
     */
    public async load(sceneName : string) : Promise<SageSceneBase>
    {
        const scene = this.scenes.get(sceneName);
        if(!scene)
        {
            throw new Error(`'Unknown scene '${ sceneName }'.`);
        } // end if

        // Load the scene, if needed
        if(!scene.loaded)
        {
            await scene.load();
            scene.loaded = true;
        } // end if

        // Set the current scene
        this.currentScene = scene;

        // Return the scene, in case someone wants to do something with it.
        return scene;
    } // end load
} // end SceneManager

//----------------------------------------------------------------------------------------------------------------------

export default new SceneManager();

//----------------------------------------------------------------------------------------------------------------------
