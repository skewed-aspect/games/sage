//----------------------------------------------------------------------------------------------------------------------
// StateManager
//----------------------------------------------------------------------------------------------------------------------

/**
 * A very simple state manager, mimicking a [[Map]]. Provided since it's a singleton and available anywhere.
 * (Used for Sage internal state management.)
 */
class StateManager
{
    constructor()
    {
        this.state = new Map();
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------

    /**
     * The internal state [[Map]].
     */
    private state : Map<string, any>;

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Retrieve a key from the state store, or a default if the key isn't fount.
     *
     * @param key - The key to retrieve.
     * @param defaultVal - The default value to return if the key is not found, or undefined.
     *
     * @returns Returns the stored value, or default.
     */
    get(key : string, defaultVal : any) : any
    {
        return this.state.get(key) ?? defaultVal;
    } // end get

    /**
     * Sets a key to a specific value.
     *
     * @param key - The key to set.
     * @param val - The new value of the key.
     */
    set(key : string, val : any) : void
    {
        this.state.set(key, val);
    } // end set

    /**
     * A simple check to see if a key exists.
     *
     * @param key - The key to check.
     *
     * @returns Returns true of the key exists, otherwise false.
     */
    has(key : string) : boolean
    {
        return this.state.has(key);
    } // end has
} // end StateManager

//----------------------------------------------------------------------------------------------------------------------

export default new StateManager();

//----------------------------------------------------------------------------------------------------------------------
