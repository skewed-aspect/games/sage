//----------------------------------------------------------------------------------------------------------------------
// SageSceneBase
//----------------------------------------------------------------------------------------------------------------------

import { v4 } from 'uuid';
import {
    AbstractMesh,
    Color3,
    CubeTexture,
    Mesh,
    MeshBuilder,
    Scene,
    SceneLoader,
    StandardMaterial,
    Texture
} from 'babylonjs';

// Managers
import engineMan from './managers/engine';
import stateMan from './managers/state';

//----------------------------------------------------------------------------------------------------------------------

/**
 * The base class for writing scenes.
 *
 * Instead of trying to build a DSL for scenes, Sage has the developer just write code, giving a lot more flexibility
 * to building levels, and allowing for a higher degree of interactivity. (This class provides several convenience
 * function for building levels as well.)
 *
 * The intention is for users to inherit from this class and implement the `load()` function.
 */
export abstract class SageSceneBase
{
    constructor()
    {
        this.loadedMeshes = new Map();
        this.scene = new Scene(engineMan.engine);
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Protected Properties
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Any cached meshes from the `loadMesh` function.
     */
    protected loadedMeshes : Map<string, Mesh>;

    /**
     * The internal BabylonJS scene.
     */
    protected readonly scene : Scene;

    //------------------------------------------------------------------------------------------------------------------
    // Public Properties
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Has this scene been loaded already? (If so, the manager will skip calling `load()`.)
     */
    public loaded = false;

    //------------------------------------------------------------------------------------------------------------------
    // Protected Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * A convenience function for creating sky boxes. This function is opinionated (in that it sets some colors), but
     * that can be override by the caller once the mesh is returned.
     *
     * @param baseFilename - The path (relative to `assetRootDir`) and base filename of the texture. (Assumes 6 texture
     * files with a common naming convention, i.e. `skybox_top.png`, `skybox_bottom.png`, etc.)
     *
     * @param extensions - An array of the texture extensions (the non-common part, including the file extension) for
     * the texture files. In the order of `[ px, py, pz, nx, ny, nz ]`.
     *
     * @returns Returns the box mesh for the skybox.
     */
    protected createSkybox(baseFilename : string, extensions : string[]) : AbstractMesh
    {
        const assetRootDir = stateMan.get('assetRootDir', '');

        if(!extensions)
        {
            extensions = [
                '_right1.png', // px
                '_top3.png', // py
                '_front5.png', // pz
                '_left2.png', // nx
                '_bottom4.png', // ny
                '_back6.png' // nz
            ];
        } // end if

        // Create a skybox material
        const skyboxMaterial = new StandardMaterial('skyBox', this.scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.diffuseColor = new Color3(0, 0, 0);
        skyboxMaterial.specularColor = new Color3(0, 0, 0);
        skyboxMaterial.reflectionTexture = new CubeTexture(`file://${ assetRootDir }/${ baseFilename }`, this.scene, extensions, true);
        skyboxMaterial.reflectionTexture.coordinatesMode = Texture.SKYBOX_MODE;

        // Create the actual skybox
        const skybox = MeshBuilder.CreateBox(`skyBox_${ baseFilename }`, { size: 100000.0 }, this.scene);

        // This keeps the mesh a fixed distance from the camera.
        skybox.infiniteDistance = true;

        // Set Skybox material
        skybox.material = skyboxMaterial;

        return skybox;
    } // end createSkybox

    /**
     * Retrieves a mesh instance for the given meshName, and modelPath. If the mesh has already been loaded, we retrieve
     * it from an internal cache, and return a new instance of the mesh. If it has not been loaded, we expect modelPath
     * to exist, and then we load the mesh, and store it.
     *
     * @param meshName - The name of the mesh to create an instance of. If `""`, modelPath is required, and all meshes
     * in the model will be loaded. (These meshes will not be cached, and will be loaded every time.)
     *
     * @param baseFilename - The path to the model, relative to `assetRootDir`.
     *
     * @returns Returns a promise that resolves with the new mesh instance.
     */
    protected async loadMesh(meshName : string, baseFilename : string) : Promise<AbstractMesh>
    {
        let mesh = this.loadedMeshes.get(meshName);
        if(mesh)
        {
            const instance = mesh.createInstance(v4());

            // Instances reset their parent's renderingGroupId, so we need to fix it.
            instance.renderingGroupId = 1;

            return instance;
        }
        else
        {
            const assetRootDir = stateMan.get('assetRootDir', '');
            const { meshes } = await SceneLoader.ImportMeshAsync(meshName, `file://${ assetRootDir }/${ baseFilename }`);

            // We only support loading one mesh
            mesh = meshes[0] as Mesh;

            // We put all meshes (except skyboxes) in rendering group 1.
            mesh.renderingGroupId = 1;

            if(meshName !== '')
            {
                this.loadedMeshes.set(meshName, mesh);
            } // end if

            return mesh;
        } // end if
    } // end loadMesh

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * The load function is called when a scene is loaded for the first time. It is expected to contain all code
     * required to setup the scene (with the exception of creating the scene, since that is done by this class's
     * constructor).
     */
    public async abstract load() : Promise<void>;

    /**
     * Just a pass through to the babylon scene's render function.
     */
    public render() : void
    {
        this.scene.render();
    } // end render
} // end SageSceneBase

//----------------------------------------------------------------------------------------------------------------------
