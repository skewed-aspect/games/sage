// ---------------------------------------------------------------------------------------------------------------------
// main.ts
// ---------------------------------------------------------------------------------------------------------------------

/**
 * Generates an html template for the main window.
 *
 * @param fontCSS - The style tag for any custom fonts
 *
 * @returns Returns a buffer of the html template.
 */
export function mainWindow(fontCSS) : Buffer
{
    return Buffer.from(`<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>MainWindow</title>
        <style>
            html, body {
                overflow: hidden;
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
            }

            #renderCanvas {
                width: 100%;
                height: 100%;
                touch-action: none;
            }
        </style>
        ${ fontCSS }
        <script>
            const { ipcRenderer } = require('electron');
            ipcRenderer.on('load-game', (_event, gameScript) =>
            {
                console.info(\`Loading game script '\${ gameScript }'...\`);

                // We require the game script, and then create a new class instance.
                const game = require(gameScript).default;
                const canvas = document.getElementById('renderCanvas');

                // Start the game
                game.start(canvas);
            });
        </script>
    </head>
    <body>
        <canvas id="renderCanvas"></canvas>
    </body>
</html>`);
} // end mainWindow

// ---------------------------------------------------------------------------------------------------------------------
